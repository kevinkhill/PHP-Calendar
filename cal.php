<?php

class Calendar {

    public function __construct()
    {
        $this->today        = time();

        $this->day          = date('d', time());
        $this->month        = date('m', time());
        $this->year         = date('Y', time());

        $this->firstDay     = mktime(0, 0, 0, $this->month, 1, $this->year);
        $this->monthTitle   = date('F', time());
        $this->dayCount     = cal_days_in_month(0, $this->month, $this->year) ;

        $this->dateData     = range(1, $this->dayCount);

        $this->dayOfWeekAbv = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

        $this->currentDayOfMonth = 1;
        $this->dayNumOfWeek     = 1;

        $this->blankFill = '';
        $this->output    = '<tr>';
    }

    public function getHtml() {
        $this->preBlankFill()->datesFill()->postBlankFill();

        return $this->output;
    }

    public function setDateData($dayNum, $data)
    {
        $this->dateData[$dayNum] = $data;

        return $this;
    }

    private function preBlankFill()
    {
        switch(date('D', $this->firstDay))
        {
            case 'Sun': $this->blankCount = 0; break;
            case 'Mon': $this->blankCount = 1; break;
            case 'Tue': $this->blankCount = 2; break;
            case 'Wed': $this->blankCount = 3; break;
            case 'Thu': $this->blankCount = 4; break;
            case 'Fri': $this->blankCount = 5; break;
            case 'Sat': $this->blankCount = 6; break;
        }

        while ($this->blankCount > 0) {
            $this->output .= '<td>' . $this->blankFill . '</td>';
            $this->blankCount--;
            $this->dayNumOfWeek++;
        }

        return $this;
    }

    private function datesFill()
    {
        while($this->currentDayOfMonth <= $this->dayCount) {
            $this->output .= '<td>';
            $this->output .= $this->dateData[$this->currentDayOfMonth-1];
            $this->output .= '</td>';

            $this->dayNumOfWeek++;
            $this->currentDayOfMonth++;

            if ($this->dayNumOfWeek > 7) {
                $this->output .= '</tr><tr>';
                $this->dayNumOfWeek = 1;
            }
        }

        return $this;
    }

    private function postBlankFill()
    {
        while($this->dayNumOfWeek > 1 && $this->dayNumOfWeek <= 7) {
            $this->output .= '<td>' . $this->blankFill . '</td>';
            $this->dayNumOfWeek++;
        }

        $this->output + '</tr>';

        return $this;
    }
}
